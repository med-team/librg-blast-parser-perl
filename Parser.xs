/*  Copyright (C) 2012 by Laszlo Kajan, Technical University of Munich, Germany

    This program is free software; you can redistribute it and/or modify
    it under the same terms as Perl itself, either Perl version 5.8.8 or,
    at your option, any later version of Perl 5 you may have available.  */
#ifdef __cplusplus
extern "C" {
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"
}
#endif

/* include your class headers here */
#include <rostlab/blast-parser-driver.h>

using namespace rostlab::blast;

/* We need one MODULE... line to start the actual XS section of the file.
 * The XS++ preprocessor will output its own MODULE and PACKAGE lines */
MODULE = RG::Blast::Parser	PACKAGE = RG::Blast::Parser

## The include line executes xspp with the supplied typemap and the
## xsp interface code for our class.
## It will include the output of the xsubplusplus run.

INCLUDE_COMMAND: $^X -MExtUtils::XSpp::Cmd -e xspp -- --typemap=typemap.xsp Parser.xsp

