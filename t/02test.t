use strict;
use warnings;

use Test::More tests => 23;
#use Data::Dumper;
BEGIN { use_ok('RG::Blast::Parser') };

# /usr/share/doc/libextutils-xspp-perl/examples/Object-WithIntAndString/t/02test.t
SCOPE: {
  open(EXAMPLE, '<', '/dev/null') || confess($!);
  my $o = RG::Blast::Parser->new( \*EXAMPLE );
  check_obj($o);

  is($o->result(), undef);

  my $res = $o->parse();
  is($res, undef);
  close(EXAMPLE);
}

SCOPE: {
  open(EXAMPLE, '<', 'examples/converged.ali') || confess($!);
  my $o = RG::Blast::Parser->new( \*EXAMPLE );
  check_obj($o);

  my $res = $o->parse();
  is($res->{hits}->[1]->{hsps}->[0]->{bit_score}, 226);
  undef $res;

  $res = $o->result();
  is($res->{hits}->[0]->{hsps}->[0]->{raw_score}, 539);
  #warn( Dumper($res) );
  close(EXAMPLE);
}

SCOPE: {
  open(EXAMPLE, '<', 'examples/badformat.ali') || confess($!);
  my $o = RG::Blast::Parser->new( \*EXAMPLE, "examples/badformat.ali" );
  check_obj($o);

  eval {
      my $res = $o->parse();
  };
  is($@, "parser error at ".__FILE__." line ". (__LINE__-2) .".\n");
  close(EXAMPLE);
}

SCOPE: {
  open(EXAMPLE, '<', 'examples/twores.ali') || confess($!);
  my $o = RG::Blast::Parser->new( \*EXAMPLE, "examples/twores.ali" );
  check_obj($o);

  my $resnum = 0;
  while( my $res = $o->parse() )
  {
    ++$resnum;
  }
  is($resnum, 2);
  close(EXAMPLE);
}

SCOPE: {
  open( my $oldin, "<&STDIN" ) || confess($!);
  open( STDIN, '<', 'examples/converged.ali') || confess($!);
  my $o = RG::Blast::Parser->new();
  check_obj($o);

  my $res = $o->parse();
  is($res->{hits}->[1]->{hsps}->[0]->{bit_score}, 226);
  #warn( Dumper($res) );

  open( STDIN, '<&', $oldin ) || confess($!);
}

#SCOPE: {
#  open(EXAMPLE, '<', '/home/kajla/project/rostlab/DEBIAN/squashblast/trunk/examples/hsps.ali') || confess($!);
#  my $o = RG::Blast::Parser->new( \*EXAMPLE, "examples/hsps.ali" );
#  check_obj($o);
#
#  my $res = $o->parse();
#
#  warn( Dumper($res) );
#}

sub check_obj {
  my $o = shift;
  isa_ok($o, 'RG::Blast::Parser' );
  can_ok($o, qw(new get_trace_scanning parse result set_trace_scanning) );
  foreach (qw|trace_scanning|){ ok(!$o->can($_)); }
}

# vim:et:ts=4:ai:
